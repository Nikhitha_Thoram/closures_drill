function cacheFunction(cb) {
  if (typeof cb !== "function") {
    return;
  }
  const cache = {};
  function invoke(...params) {
    const key = JSON.stringify(...params);
    if (!(key in cache)) {
      const result = cb(...params);
      cache[key] = result;
      return result;
    }
    return cache[key];
  }
  return invoke;
}
module.exports = cacheFunction;
