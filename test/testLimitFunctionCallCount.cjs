const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function cb() {
  return "Hi";
}

let res1 = limitFunctionCallCount(cb, 3);
console.log(res1());
console.log(res1());
console.log(res1());
console.log(res1());

let ans = limitFunctionCallCount();
console.log(ans);
