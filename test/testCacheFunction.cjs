const cacheFunction = require("../cacheFunction.cjs");

function cb(x, y) {
  console.log("cb invoked with parameters:", x, y);
  return x + y;
}

const result = cacheFunction(cb);
console.log(result(1, 2));
console.log(result(1, 2));
console.log(result(3, 4));
