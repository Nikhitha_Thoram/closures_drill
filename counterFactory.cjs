function counterFactory() {
  let count = 0;
  let counterObject = {
    increment: function () {
      count++;
      return count;
    },
    decrement: function () {
      count--;
      return count;
    },
  };
  return counterObject;
}

module.exports = counterFactory;
